<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class AlunoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'nome' => 'required|string|max:255',
            'cpf' => 'required|string|max:14|unique:alunos,cpf,' . $this->aluno,
            'sexo' => 'required|in:Masculino,Feminino',
            'data_nascimento' => 'required|date|after_or_equal:1900-01-01',
            'email' => 'required|email|max:255|unique:alunos,email,' . $this->aluno,
            'renda_mensal' => 'nullable|numeric|min:0',
        ];
    }


    public function messages()
    {
        return [
            'nome.required' => 'O campo nome é obrigatório.',
            'cpf.required' => 'O campo CPF é obrigatório.',
            'cpf.unique' => 'Este CPF já está sendo usado.',
            'sexo.required' => 'O campo sexo é obrigatório.',
            'data_nascimento.required' => 'O campo data de nascimento é obrigatório.',
            'data_nascimento.after_or_equal' => 'A data de nascimento deve ser maior ou igual a 01/01/1900.',
            'email.required' => 'O campo email é obrigatório.',
            'email.unique' => 'Este email já está sendo usado.',
            'renda_mensal.numeric' => 'A renda mensal deve ser um número.',
            'renda_mensal.min' => 'A renda mensal deve ser maior que zero.',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}

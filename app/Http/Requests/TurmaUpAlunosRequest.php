<?php

namespace App\Http\Requests;

// Importações de classes necessárias.
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

// Definição da classe TurmaUpAlunosRequest que herda da classe FormRequest do Laravel.
class TurmaUpAlunosRequest extends FormRequest
{
    // Método para verificar se o usuário está autorizado a fazer a requisição.
    public function authorize(): bool
    {
        // Este método está retornando 'true', o que significa que qualquer usuário pode fazer a requisição.
        return true;
    }

    // Método para definir as regras de validação para a requisição.
    public function rules(): array
    {
        return [
            // A chave 'turma_id' é obrigatória, deve ser um inteiro e tem uma função de validação personalizada.
            'turma_id' => [
                'required',  // Deve estar presente na requisição.
                'integer',   // Deve ser um número inteiro.
                function ($attribute, $value, $fail) {
                    // Consulta no banco de dados para encontrar a turma com o ID fornecido.
                    $turma = DB::table('turmas')->where('id', $value)->first();
                    if ($turma) {
                        // Conta o número de alunos atualmente na turma.
                        $currentAlunosCount = DB::table('aluno_turma')->where('turma_id', $value)->count();
                        // Verifica se o número de alunos é maior do que a capacidade máxima da turma.
                        if ($currentAlunosCount > $turma->quantidade_maxima_alunos) {
                            // Se for, falha a validação com uma mensagem de erro.
                            $fail('A turma atingiu a capacidade máxima de alunos.');
                        }
                    } else {
                        // Se a turma não existir, falha a validação com uma mensagem de erro.
                        $fail('A turma não existe.');
                    }
                }
            ],
            // A chave 'alunos' também é obrigatória, deve ser um array e tem uma função de validação personalizada.
            'alunos' => [
                'required',  // Deve estar presente na requisição.
                'array',     // Deve ser um array.
                function ($attribute, $value, $fail) {
                    $turmaId = $this->input('turma_id');
                    $turma = DB::table('turmas')->where('id', $turmaId)->first();
                    if ($turma) {
                        $totalAlunos = count($value);
                        // Verifica se o número de alunos enviados é maior que a capacidade da turma.
                        if ($totalAlunos > $turma->quantidade_maxima_alunos) {
                            $fail('A quantidade de alunos excede a capacidade máxima da turma.');
                        }
                    }
                }
            ],
            // Verifica se cada aluno (ID) é um número inteiro e existe na tabela 'alunos'.
            'alunos.*' => [
                'integer',   // Deve ser um número inteiro.
                Rule::exists('alunos', 'id'),  // Deve existir na tabela 'alunos'.
            ],
        ];
    }

    // Método para definir as mensagens de erro personalizadas para as regras de validação.
    public function messages()
    {
        return [
            'turma_id.required' => 'O ID da turma é obrigatório.',
            'turma_id.integer' => 'O ID da turma deve ser um número inteiro.',
            'alunos.required' => 'É necessário fornecer ao menos um aluno.',
            'alunos.array' => 'Os alunos devem ser fornecidos em um array.',
            'alunos.*.integer' => 'Os IDs dos alunos devem ser números inteiros.',
            'alunos.*.exists' => 'Um ou mais alunos fornecidos não existem.',
        ];
    }

    // Método chamado quando a validação falha.
    protected function failedValidation(Validator $validator)
    {
        // Lança uma exceção com uma resposta HTTP contendo os erros de validação.
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class TurmaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'codigo_turma' => 'required|string|max:255',
            'data_inicio' => 'required|date|after_or_equal:today',
            'data_fim' => 'required|date|after:data_inicio',
            'quantidade_maxima_alunos' => 'required|integer|min:1',
        ];
    }

    public function messages()
    {
        return [
            'codigo_turma.required' => 'O campo código da turma é obrigatório.',
            'data_inicio.required' => 'O campo data de início é obrigatório.',
            'data_inicio.after_or_equal' => 'A data de início deve ser maior ou igual a data atual.',
            'data_fim.required' => 'O campo data de fim é obrigatório.',
            'data_fim.after' => 'A data de fim deve ser maior que a data de início.',
            'quantidade_maxima_alunos.required' => 'O campo quantidade máxima de alunos é obrigatório.',
            'quantidade_maxima_alunos.min' => 'A quantidade máxima de alunos deve ser maior que zero.',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}

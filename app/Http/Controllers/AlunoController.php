<?php

namespace App\Http\Controllers;

use App\Http\Requests\AlunoRequest;
use App\Models\Aluno;
use Exception;
use Illuminate\Http\Request;

class AlunoController extends Controller
{

    public function index(Request $req)
    {
        $search = $req->search ?? '';
        $perPage = $req->perPage ?? 20;

        try {
            $list = Aluno::where(function ($query) use ($search, $req) {

                if ($req->id) {
                    $query->where(['id' => $req->id]);
                } else {

                    $query->where('nome', 'like', '%' . $search . '%')
                        ->orWhere('cpf', 'like', '%' . $search . '%')
                        ->orWhere('sexo', 'like', '%' . $search . '%')
                        ->orWhere('email', 'like', '%' . $search . '%')
                        ->orWhere('renda_mensal', 'like', '%' . $search . '%');
                }
            })
                ->orderBy('id', 'desc')
                ->paginate($perPage);
            $list->load('turmas');
            return response()->json($list);
        } catch (Exception $e) {
            return response()->json(['status' => 0, 'msg' => $e->getMessage()]);
        }
    }

    public function store(AlunoRequest $req)
    {
        try {
            $add = new Aluno();
            $add->nome = $req->nome;
            $add->cpf = $req->cpf;
            $add->sexo = $req->sexo;
            $add->data_nascimento = $req->data_nascimento;
            $add->email = $req->email;
            $add->renda_mensal = $req->renda_mensal;
            if ($add->save()) {
                return response()->json(['status' => 1, 'msg' => 'Novo aluno Cadastrado com sucesso.']);
            }
            return response()->json(['status' => 0, 'msg' => 'tente novamente.']);
        } catch (Exception $e) {
            return response()->json(['status' => 0, 'msg' => $e->getMessage()]);
        }
    }

    function update(Request $req, string $id)
    {
        try {
            $update = Aluno::where(['id' => $id])->update([
                'nome' => $req->nome,
                'cpf' => $req->cpf,
                'sexo' => $req->sexo,
                'data_nascimento' => $req->data_nascimento,
                'email' => $req->email,
                'active' => $req->active,
                'renda_mensal' => $req->renda_mensal,
            ]);
            if ($update) {
                return response()->json(['status' => 1, 'msg' => 'Dados atualizado com sucesso.']);
            }
            return response()->json(['status' => 0, 'msg' => 'Tente novamente.']);
        } catch (Exception $e) {
            return response()->json(['status' => 0, 'msg' => $e->getMessage()]);
        }
    }
}

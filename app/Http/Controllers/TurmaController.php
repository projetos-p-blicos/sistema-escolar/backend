<?php

namespace App\Http\Controllers;

use App\Http\Requests\TurmaRequest;
use App\Http\Requests\TurmaUpAlunosRequest;
use App\Models\Turma;
use Exception;
use Illuminate\Http\Request;


class TurmaController extends Controller
{
    public function index(Request $req)
    {
        $search = $req->search ?? '';
        $perPage = $req->perPage ?? 20;
        try {
            $list = Turma::where(function ($query) use ($search, $req) {
                if ($req->id) {
                    $query->where(['id' => $req->id]);
                } else {
                    $query->where('codigo_turma', 'like', '%' . $search . '%')
                        ->orWhere('quantidade_maxima_alunos', 'like', '%' . $search . '%');
                }
            })
                ->orderBy('id', 'desc')
                ->paginate($perPage);
            $list->load('alunos');
            return response()->json($list);
        } catch (Exception $e) {
            return response()->json(['status' => 0, 'msg' => $e->getMessage()]);
        }
    }

    public function store(TurmaRequest $req)
    {
        try {
            $add = new Turma();
            $add->codigo_turma = $req->codigo_turma;
            $add->data_inicio = $req->data_inicio;
            $add->data_fim = $req->data_fim;
            $add->quantidade_maxima_alunos = $req->quantidade_maxima_alunos;
            if ($add->save()) {
                return response()->json(['status' => 1, 'msg' => 'Nova turma cadastrada com sucesso.']);
            }
            return response()->json(['status' => 0, 'msg' => 'tente novamente.']);
        } catch (Exception $e) {
            return response()->json(['status' => 0, 'msg' => $e->getMessage()]);
        }
    }




    public  function update(Request $req, $id)
    {
        try {
            $update = Turma::where(['id' => $id])->update([
                'codigo_turma' => $req->codigo_turma,
                'data_inicio' => $req->data_inicio,
                'data_fim' => $req->data_fim,
                'quantidade_maxima_alunos' => $req->quantidade_maxima_alunos,
            ]);
            if ($update) {
                return response()->json(['status' => 1, 'msg' => 'Dados atualizado com sucesso.']);
            }
            return response()->json(['status' => 0, 'msg' => 'Tente novamente.']);
        } catch (Exception $e) {
            return response()->json(['status' => 0, 'msg' => $e->getMessage()]);
        }
    }


    public function addAlunoTurma(TurmaUpAlunosRequest $req)
    {
        $upAlunoTurma = Turma::find($req->turma_id);
        try {
            if ($upAlunoTurma) {
                // Atualizar as veiculos do usuário.
                $upAlunoTurma->alunos()->sync($req->alunos);

                $upAlunoTurma->load('alunos');

                return response()->json(['status' => 1, 'msg' => 'Alunos atualizado com sucesso.']);
            }
            return response()->json(['status' => 0, 'msg' => 'Alunos não encontrado.']);
        } catch (Exception $e) {
            return response()->json(['status' => 0, 'msg' => 'error tente novamente', 'error' => $e->getMessage()]);
        }
    }
}

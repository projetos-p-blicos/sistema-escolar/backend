<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('aluno_turma')) {
            Schema::create('aluno_turma', function (Blueprint $table) {

                $table->increments('id');
                $table->unsignedInteger('aluno_id');
                $table->unsignedInteger('turma_id');
                $table->timestamps();

                $table->foreign('aluno_id')->references('id')->on('alunos');
                $table->foreign('turma_id')->references('id')->on('turmas');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('aluno_turma');
    }
};

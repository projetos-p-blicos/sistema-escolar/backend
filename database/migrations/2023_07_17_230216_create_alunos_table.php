<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('alunos')) {
            Schema::create('alunos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nome');
                $table->string('cpf')->unique();
                $table->enum('sexo', ['Masculino', 'Feminino']);
                $table->date('data_nascimento');
                $table->string('email')->unique();
                $table->boolean('active')->default(1);
                $table->decimal('renda_mensal', 8, 2)->nullable();
                $table->timestamps();
            });
        }
    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('alunos');
    }
};

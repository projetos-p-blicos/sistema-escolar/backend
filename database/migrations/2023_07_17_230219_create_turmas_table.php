<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('turmas')) {
            Schema::create('turmas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('codigo_turma');
                $table->date('data_inicio');
                $table->date('data_fim');
                $table->integer('quantidade_maxima_alunos');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('turmas');
    }
};

<?php

use App\Http\Controllers\AlunoController;
use App\Http\Controllers\TurmaController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;


Route::prefix('alunos')->controller(AlunoController::class)->group(function () {
    Route::get('', 'index');
    Route::post('', 'store');
    Route::put('{id}', 'update');
    Route::delete('{id}', 'delete');
});


Route::prefix('turmas')->controller(TurmaController::class)->group(function () {
    Route::get('', 'index');
    Route::post('', 'store');
    Route::put('{id}', 'update');
    Route::delete('{id}', 'delete');
    Route::post('add_aluno_turma', 'addAlunoTurma');
});
